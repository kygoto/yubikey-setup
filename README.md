Enable yubikey authentication

# Verify
diff <(sha256sum *.deb) <(cat sha256.txt)

# Modify bashrc
./setup.sh

# Install packages (dpkg)
libfl2
libccid             *
libyubikey0
libykpers
libyubikey-udev
python3
  colorama
  click
  fido2
  gnupg
  openssl
  pyscard
  usb
  yubikey-manager
at
gnupg2              *
gnupg-agent         *
  libatomic1 (apt)
hopenpgp
pcscd               *
rng-tools
scdaemon            *
secure-delete
yubikey-manager
yubikey-personalization

# Import public key
gpg --import ${PUBKEY}

# Trust (Ultimate 5)
gpg --edit-key 0x${KEYID}

