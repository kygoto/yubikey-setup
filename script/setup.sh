#!/bin/bash

tee -a "${HOME}/.bashrc" << TXT

#### GPG-SSH Configuration ###
if command -v gpgconf &> /dev/null; then
  export GPG_TTY="$(tty)"
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
  gpgconf --launch gpg-agent
fi

# gpg-connect-agent updatestartuptty /bye > /dev/null
# alias ssh="gpg-connect-agent updatestartuptty /bye > /dev/null; ssh"
# alias scp="gpg-connect-agent updatestartuptty /bye > /dev/null; scp"

TXT

